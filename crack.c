#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any plaintext password will be


// Stucture to hold both a plaintext password and a hash.
struct entry 
{
    char plain[PASS_LEN];
    char hash[33];
};


// Compare entries by hash value
int ecompare(const void *a, const void *b)
{
    struct entry *aa = (struct entry*)a;
    struct entry *bb = (struct entry*)b;
    return strcmp(aa->hash, bb->hash);
}


// Search entries array for hash
int esearch(const void *t, const void *e)
{
    char *tt = (char *)t;
    struct entry *ee = (struct entry*)e;
    return strcmp(tt, ee->hash);
}


// Read in the dictionary file and return an array of entry structs.
// Each entry should contain both the hash and the dictionary word (password).
struct entry * read_dictionary(char *filename, int *size)
{
    // Open the dictionary file
    FILE *file = fopen(filename, "r");
    if (!file)
    {
        fprintf(stderr, "Can't open %s for reading\n", filename);
        perror(NULL);
        exit(1);
    }

    // Allocate memory for array of enry structs
    int capacity = 10;
    struct entry * entries = malloc(capacity * sizeof(struct entry));
    
    // Loop through dictionary file, store each line as plaintest and hashes into entry structures
    int count = 0;
    char line[PASS_LEN];
    while (fgets(line, PASS_LEN, file) != NULL)
    {
        // Trim newline
        char *nl = strchr(line, '\n');
        if (nl) *nl = '\0';

        // Add plaintext and equivalent hash to array
        strcpy(entries[count].plain, line);
        char *lineHash = md5(line, strlen(line));
        strcpy(entries[count].hash, lineHash);
        count++;

        // Free up malloced memory used for hash
        free(lineHash);

        // Expand entries array when capacity is reached
        if (count == capacity)
        {
            // Make allocated array bigger
            capacity += 10;
            entries = realloc(entries, capacity * sizeof(struct entry));
        }
    }
    
    // Close the dictionary file
    fclose(file);
    
    // Update size of dictionary
    *size = count;
    
    // Return pointer to the array of entries
    return entries;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of entry structures
    int dlen;
    struct entry *dict = read_dictionary(argv[2], &dlen);

   
    // Sort the hashed dictionary using qsort
    qsort(dict, dlen, sizeof(struct entry), ecompare);


    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");
    if (!hashFile)
    {
        fprintf(stderr, "Can't open %s for reading\n", argv[1]);
        perror(NULL);
        exit(1);
    }

    // For each hash, search for it in the dictionary
    char hash[33];
    int i = 0;
    while (fgets(hash, 33, hashFile))
    {
        struct entry * found = bsearch(hash, dict, dlen, sizeof(struct entry), esearch);
        if (found)
        {
            // get corresponding plaintext word,
            // print out both the hash and corresponding plaintext word
            printf("(%d) %s %s", i, found->hash, found->plain);
            printf("\n");
            i++;
        }
    }

    
    // Free up allocated memory in hash dictionary and close the hash file
    free(dict);
    fclose(hashFile);
}
